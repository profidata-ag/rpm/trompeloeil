%undefine __cmake_in_source_build
%global debug_package %{nil}

Name:           trompeloeil
Version:        41
Release:        1%{?dist}
Summary:        Header only C++14 mocking framework

License:        Boost
URL:            https://github.com/rollbear/%{name}
Source0:        %{url}/archive/v%{version}/%{name}-%{version}.tar.gz

%if 0%{?rhel}
BuildRequires:  gcc-toolset-10
%else
BuildRequires:  gcc-c++
%endif
BuildRequires: cmake

%description
A thread-safe header-only mocking framework for
C++11/14 using the Boost Software License 1.0

%package        devel
Summary:        Development files for %{name}
Requires:       catch-devel

%description    devel
A thread-safe header-only mocking framework for
C++11/14 using the Boost Software License 1.0

%prep
%setup -qn %{name}-%{version}

%build
sed -i 's@lib/cmake/trompeloeil@lib64/cmake/trompeloeil@g' CMakeLists.txt
%if 0%{?rhel}
scl enable gcc-toolset-10 - << \SCLEOF
set -e
%endif
%cmake -DCMAKE_BUILD_TYPE=Debug
%if 0%{?rhel}
SCLEOF
%endif

%check
%cmake_build --target run_self_test

%install
%cmake_install
rm -r %{buildroot}%{_includedir}/{boost,crpcut,cxxtest,doctest,gtest,criterion}

%files devel
%{_docdir}/%{name}/
%{_libdir}/cmake/%{name}/
%{_includedir}/trompeloeil.hpp
%{_includedir}/catch2/trompeloeil.hpp
